package eu.unbekandt.assignment;

/**
 * Represent a mathematical matrix
 * @author leo
 */
public class Matrix {

    private double[] v;
    private int[] pvts;
    private int nrows;
    private int ncols;

    /**
     * Default constructor (empty matrix)
     */
    public Matrix() {
        nrows = 0;
        ncols = 0;
        v = new double[0];
    }

    /**
     * Alternate constructor - creates a matrix with the given values
     * @param n width of matrix
     * @param m height of matrix
     */
    public Matrix(int n, int m) {
        //check input
        if (n < 0 || m < 0) {
            throw new IllegalArgumentException("matrix size negative");
        }
        nrows = n;
        ncols = m;

        if (nrows <= 0 || ncols <= 0) {
            v = new double[0];
            pvts = new int[0];
        } else {
            v = new double[nrows * ncols];
            pvts = new int[nrows];
        }
    }

    /**
     * Constructor from string
     * Parse it with a Matrix Parser
     * @param s
     * @return
     * @throws IllegalArgumentException
     */
    public static Matrix parseFromInput(String s) throws IllegalArgumentException {
        MatrixParser mp = new MatrixParser(s);
        return mp.validate().parse();
    }

    /**
     * Alternate constructor - creates a matrix from a vector
     * @param x
     */
    public Matrix(double[] x) {
        v = x.clone();
        pvts = new int[x.length];
        nrows = x.length;
        ncols = 1;
    }

    /**
     * Copy constructor
     * @param m original matrix
     */
    public Matrix(Matrix m) {
        v = m.v.clone();
        pvts = m.pvts.clone();
        nrows = m.getNrows();
        ncols = m.getNcols();
    }

    // ACCESSOR METHODS
    /**
     * @return number of rows of the matrix
     */    
    public int getNrows() {
        return nrows;
    }
    
    /**
     * @return number of columns of the matrix
     */
    public int getNcols() {
        return ncols;
    }

    /**
     * @return array of pivots to apply Gauss algorithm
     */    
    public int[] getPvts() {
        return this.pvts;
    }
    
    /**
     * Set an element of the matrix
     * @param i
     * @param j
     * @param value
     * @return this to chain operations
     */
    public Matrix set(int i, int j, double value) {
        if (i > nrows - 1 || j > ncols - 1 || i < 0 || j < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        v[i * ncols + j] = value;
        return this;
    }

    /**
     * Get an element of the matrix
     * @param i
     * @param j
     * @return the value of the element
     */
    public double get(int i, int j) {
        // if the given parameters (coordinates) are out of range
        if (i > nrows - 1 || j > ncols - 1 || i < 0 || j < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return v[i * ncols + j];
    }

    // NORMS
    /**
     * Norm I of a matrix
     * @return
     */
    public double oneNorm() {
        int i, j;
        double max, sum;

        max = 0;
        for (i = 0; i < nrows; i++) {
            sum = 0;
            for (j = 0; j < ncols; j++) {
                sum += Math.abs(this.get(i, j));
            }
            if (max < sum) {
                max = sum;
            }
        }
        return max;
    }

    /**
     * Norm II of a matrix
     * @return
     */
    public double twoNorm() {
        int i, j;
        double sum = 0;

        for (i = 0; i < nrows; i++) {
            for (j = 0; j < ncols; j++) {
                sum += Math.abs(this.get(i, j) * this.get(i, j));
            }
        }
        return (Math.sqrt(sum));
    }

    /**
     * Uniform norm of a matrix
     * @return
     */
    public double uniformNorm() {
        int i, j;
        double sum, max;

        max = 0;
        for (j = 0; j < nrows; j++) {
            sum = 0;
            for (i = 0; i < ncols; i++) {
                sum += Math.abs(this.get(i, j));
            }
            if (max < sum) {
                max = sum;
            }
        }
        return max;
    }

    /**
     * Multiplication of two matrices 
     * @param a
     * @return
     */    
    public Matrix mult(Matrix a) {
        //if the matrix sizes do not match
        if (getNcols() != a.getNrows()) {
            throw new IllegalArgumentException("matrix sizes do not match");
        }

        //return matrix
        Matrix matrix = new Matrix(nrows, a.ncols);

        //matrix multiplication
        for (int i = 0; i < matrix.nrows; i++) {
            for (int j = 0; j < matrix.ncols; j++) {
                matrix.set(i, j, 0.0);
            }
        }

        for (int i = 0; i < nrows; i++) {
            for (int j = 0; j < a.ncols; j++) {
                for (int k = 0; k < ncols; k++) {
                    matrix.set(i, j, matrix.get(i, j) + this.get(i, k) * a.get(k, j));
                }
            }
        }

        return matrix;
    }

    /**
     * Multiplication of the matrix by a vector 
     * @param v
     * @return
     */
    public double[] mult(double[] v) {

        //if the matrix sizes do not match
        if (ncols != v.length) {
            throw new IllegalArgumentException("matrix sizes do not match");
        }

        //return matrix
        double[] res = new double[nrows];

        for (int i = 0; i < nrows; i++) {
            for (int j = 0; j < ncols; j++) {
                res[i] += (this.get(i, j) * v[j]);
            }
        }

        return res;
    }

    /**
     * Comparison function, returns true if the given matrices are the same
     * @param a
     * @param tol
     * @return
     */
    public boolean equiv(Matrix a, double tol) {
        //if the sizes do not match return false*
        if ((nrows != a.nrows) || (ncols != a.ncols)) {
            return false;
        }

        //compare all of the elements
        for (int i = 0; i < nrows; i++) {
            for (int j = 0; j < ncols; j++) {
                if (Math.abs(this.get(i, j) - a.get(i, j)) > tol) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Transpose of the matrix
     * @return
     */
    public Matrix transpose() {
        Matrix temp = new Matrix(nrows, ncols);

        for (int i = 0; i < nrows; i++) {
            for (int j = 0; j < ncols; j++) {
                temp.set(i, j, this.get(j, i));
            }
        }

        return temp;
    }

    /**
     * Compute determinant of the matrix
     * @return
     */
    public double det() {
        double res = 1.0;
        Matrix p = this.reorder();
        Matrix pa = p.mult(this);
        Matrix m = pa.computeUpper();
        for (int i = 0; i < nrows; i++) {
            res *= Math.abs(m.get(i, i));
        }
        return res;
    }

    // LU

    /**
     * Compute the temporary matrix used by the computation of L and U
     * @return
     */
    public Matrix computeTempLU() {
        Matrix temp = new Matrix(this);
        int n = this.getNrows();
        int i, j, k;
        double mult;
        // LU (Doolittle's) decomposition without pivoting
        for (k = 0; k < n - 1; k++) {
            for (i = k + 1; i < n; i++) {
                if (temp.get(k, k) == 0) {
                    throw new IllegalArgumentException("pivot is 0");
                }
                mult = temp.get(i, k) / temp.get(k, k);
                temp.set(i, k, mult);                      // entries of L are saved in y
                for (j = k + 1; j < n; j++) {
                    temp.set(i, j, temp.get(i, j) - mult * temp.get(k, j));      // entries of U are saved in y
                }
            }
        }
        return temp;
    }

    /**
     * Compute lower matrix of a LU decomposition
     * @return
     */
    public Matrix computeLower() {
        Matrix l = new Matrix(this.nrows, this.ncols);
        Matrix temp = this.computeTempLU();
        int n = this.nrows;

        for (int i = 0; i < n; i++) {
            l.set(i, i, 1.0);
        }
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                l.set(i, j, temp.get(i, j));
            }
        }

        return l;
    }

    /**
     * Compute upper matrix of a LU decomposition
     * @return
     */
    public Matrix computeUpper() {
        Matrix u = new Matrix(this.nrows, this.ncols);
        Matrix temp = this.computeTempLU();
        int n = this.nrows;

        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                u.set(i, j, temp.get(i, j));
            }
        }

        return u;
    }

    /**
     * Switch two lines of the matrix
     * @param n
     * @param m 
     * @return  
     */
    public Matrix switchLines(int n, int m) {
        double tmp;
        for (int i = 0; i < this.nrows; i++) {
            tmp = this.get(n, i);
            this.set(n, i, this.get(m, i));
            this.set(m, i, tmp);
        }
        return this;
    }

    /**
     * Ln = Ln - coef * Lm
     * @param n
     * @param m
     * @param coef 
     * @return  
     */
    public Matrix subLines(int n, int m, double coef) {
        for (int i = 0; i < this.nrows; i++) {
            this.set(n, i, this.get(n, i) - (this.get(m, i) * coef));
        }
        return this;
    }

    /**
     * Ln = coef * Ln
     * @param n
     * @param coef 
     * @return  
     */
    public Matrix mulLines(int n, double coef) {
        for (int i = 0; i < this.nrows; i++) {
            this.set(n, i, this.get(n, i) * coef);
        }
        return this;
    }

    /**
     * Calculate the inverse of a matrix using Gauss-Jordan
     * @return
     */
    public Matrix inv() {
        int i, j, ii, imax;
        double max, coef;
        int n = this.nrows;
        Matrix inv = new Matrix(n, n);
        Matrix matrix = new Matrix(this);

        // Identity matrix to do Gauss-Jordan algorithm
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (i != j) {
                    inv.set(i, j, 0.0);
                } else {
                    inv.set(i, j, 1.0);
                }
            }
        }

        for (i = 0; i < n; i++) {
            max = Math.abs(matrix.get(i, i));
            imax = i;
            for (ii = i + 1; ii < n; ii++) {
                if (Math.abs(matrix.get(ii, i)) > max) {
                    max = Math.abs(matrix.get(ii, i));
                    imax = ii;
                }
            }
            if (matrix.get(imax, i) == 0) {
                throw new IllegalArgumentException("The matrix is not inversable");
            }

            if (imax != i) {
                matrix.switchLines(i, imax);
                inv.switchLines(i, imax);
            }

            coef = 1. / matrix.get(i, i);
            matrix.mulLines(i, coef);
            inv.mulLines(i, coef);

            for (ii = 0; ii < n; ii++) {
                if (ii != i && matrix.get(ii, i) != 0) {
                    coef = matrix.get(ii, i) / matrix.get(i, i);
                    matrix.subLines(ii, i, coef);
                    inv.subLines(ii, i, coef);
                }
            }
        }
        return inv;
    }

    /**
     * Compute matrix of permutations
     * @return 
     */
    public Matrix reorder() {
        int i, j, k;
        int n = this.nrows;
        Matrix p = new Matrix(this.nrows, this.ncols);

        double[] pvt = new double[n];
        double[] scale = new double[n];
        Matrix temp = new Matrix(this);

        int pvtk, pvti;
        double aet, tmp, mult;

        for (k = 0; k < n; k++) {
            pvt[k] = k;
        }
        for (k = 0; k < n; k++) {
            scale[k] = 0;
            for (j = 0; j < n; j++) {
                if (Math.abs(scale[k]) < Math.abs(temp.get(k, j))) {
                    scale[k] = Math.abs(temp.get(k, j));
                }
            }
        }

        // main elimination loop
        for (k = 0; k < n - 1; k++) {
            int pc = k;
            aet = Math.abs(temp.get((int) pvt[k], k) / scale[k]);
            for (i = k + 1; i < n; i++) {
                tmp = Math.abs(temp.get((int) pvt[i], k) / scale[(int) pvt[i]]);
                if (tmp > aet) {
                    aet = tmp;
                    pc = i;
                }
            }
            if (aet == 0) {
                throw new IllegalArgumentException("pivot is 0");
            }
            if (pc != k) {                      // swap pvt[k] and pvt[pc]
                int ii = (int) pvt[k];
                pvt[k] = pvt[pc];
                pvt[pc] = ii;
            }

            pvtk = (int) pvt[k];
            for (i = k + 1; i < n; i++) {
                pvti = (int) pvt[i];
                if (temp.get(pvti, k) != 0) {
                    mult = temp.get(pvti, k) / temp.get(pvtk, k);
                    temp.set(pvti, k, mult);
                    for (j = k + 1; j < n; j++) {
                        temp.set(pvti, j, temp.get(pvti, j) - mult * temp.get(pvtk, j));
                    }
                }
            }
        }
        for (i = 0; i < n; i++) {
            p.set(i, (int) pvt[i], 1.0);
        }
        
        for(i = 0; i < this.pvts.length; i++) {
            this.pvts[i] = (int)pvt[i]+1;
        }
        return p;
    }

    /**
     * Find x after a LU decomposition
     * @param l
     * @param u
     * @param b
     * @return
     */
    public static double[] LUSolve(Matrix l, Matrix u, double[] b) {
        int n = b.length;
        double[] x = new double[n];
        double[] y = b.clone();
        int i, j;

        for (i = 1; i < n; i++) {
            for (j = 0; j < i; j++) {
                y[i] -= l.get(i, j) * y[j];
            }
        }

        // back substitution for U x = y.  
        for (i = n - 1; i >= 0; i--) {
            for (j = i + 1; j < n; j++) {
                y[i] -= u.get(i, j) * y[j];
            }
            y[i] /= u.get(i, i);
        }

        for (i = 0; i < n; i++) {
            x[i] = y[i];
        }
        return x;
    }

    /**
     * toString of a matrix
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < nrows; i++) {
            sb.append("  ");
            for (int j = 0; j < ncols; j++) {
                sb.append(String.format("%1.6f", this.get(i, j)));
                sb.append(' ');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
