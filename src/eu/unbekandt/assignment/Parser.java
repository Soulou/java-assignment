/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.unbekandt.assignment;

/**
 *
 * @author leo
 */
public interface Parser<T> {
    Parser<T> validate() throws IllegalArgumentException;
    T parse();
}
