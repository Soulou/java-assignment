package eu.unbekandt.assignment;

/**
 * Main of the application, run the swing window
 * @author leo
 */
public class JavaAssignment {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainWindow m = new MainWindow(false);
                m.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                m.setTitle("Matrix operations");
                m.setVisible(true);
                
            }
        });
    }
    
}
