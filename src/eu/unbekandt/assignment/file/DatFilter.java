/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.unbekandt.assignment.file;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Filter for .dat file in save/load window
 * @author leo
 */
public class DatFilter implements FilenameFilter {
    @Override
    public boolean accept(File file, String string) {
        return string.endsWith(".dat");
    }
}
