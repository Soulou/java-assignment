/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.unbekandt.assignment.file;

import eu.unbekandt.assignment.MainWindow;
import java.awt.FileDialog;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Save window for application
 * @author leo
 */
public class DataSaver extends FileDialog {
    private MainWindow parent;
    
    /**
     * Main constructor to init the window
     * @param parent Parent window (to appear over the parent window)
     */
    public DataSaver(MainWindow parent) {
        super(parent, "Save workspace");
        this.parent = parent;
        
        setMode(FileDialog.SAVE);
        setFilenameFilter(new DatFilter());
        setFile("matrix_workspace.dat"); 
    }
    
    /**
     * Action when the save window is closed
     */
    public void save() {
        setVisible(true);
        
        String file = getFile();
        if(file != null) {
            saveWorkspace(file);
        }
    }
    
    /**
     * Dump wokspace from main window and write the output file
     * @param f Path of the save file
     */
    private void saveWorkspace(String f) {
        try {
            FileWriter file = new FileWriter(f, false);
            file.append(Constants.HEADER);
            file.append(parent.toString());
            file.close();
        } catch(IOException e) {
            this.parent.setError("Fail to save " + f + ": " + e.getMessage());
        }
    }
}
