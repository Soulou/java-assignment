/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.unbekandt.assignment.file;

import eu.unbekandt.assignment.MainWindow;
import java.awt.FileDialog;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Load window for application
 * @author leo
 */
public class DataLoader extends FileDialog {
    private final MainWindow parent;
    
    /**
     * Main constructor to init the window
     * @param parent Parent window (to appear over the parent window)
     */
    public DataLoader(MainWindow parent) {
        super(parent, "Load workspace");
        this.parent = parent;
        
        setMode(FileDialog.LOAD);
        setFilenameFilter(new DatFilter());
        setFile("matrix_workspace.dat");
    }
    
    /**
     * Action when the load window is closed
     */
    public void load() {
        setVisible(true);
        String f = getFile();
        if(f != null) {
            loadWorkspace(f);
        }
    }
    
    /**
     * Load workspace from file and fill parent window
     * @param f Path of the selected file
     */
    private void loadWorkspace(String f) {
        try {
            FileReader r = new FileReader(f);
            char[] reader = new char[7];
            r.read(reader);
            if(!(new String(reader).equals(Constants.HEADER))) {
                throw new IOException("Invalid header in file");
            }
            
            Scanner sc = new Scanner(r);
            String current;
            int length;
            char type = '\0';
            
            StringBuilder content = new StringBuilder();
            while(sc.hasNext()) {
                current = sc.nextLine();
                if(current.length() >= 3 && current.substring(0, 3).equals("###")) {
                    if(content.length() > 0) {
                        this.parent.setContent(type, content.toString());
                        content = new StringBuilder();
                    }
                    type = current.charAt(3);
                } else {
                    content.append(current);
                    content.append('\n');
                }
            }
            this.parent.setContent(type, content.toString());
        } catch (IOException e) {
            this.parent.setError("Fail to load file " + f + ": " + e.getMessage());
        }
    }
}
