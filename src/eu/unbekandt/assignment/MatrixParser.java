package eu.unbekandt.assignment;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Implementation of Parser for a Matrix
 * @author leo
 */
public class MatrixParser implements Parser<Matrix> {
    private final String input;
    private int width;
    private int height;
    
    /**
     * Constructor takes string input
     * @param in
     */
    public MatrixParser(String in) {
      this.input = in;
      this.width = -1;
      this.height = -1;
    }
    
    /**
     * Validate if the input is correct or throw exception
     * @return
     * @throws IllegalArgumentException
     */
    @Override
    public MatrixParser validate() throws IllegalArgumentException {
        Scanner scLine = new Scanner(this.input);
        int _height = 0, _width = 0, prevWidth = 0;
        String line;
        while(scLine.hasNextLine()) {
            line = scLine.nextLine();
            _height++;
            try {
                _width = parseLine(line);
            } catch (NoSuchElementException e) {
                throw new IllegalArgumentException("contains something else than numbers");
            }
            if(prevWidth != _width && prevWidth != 0) {
                throw new IllegalArgumentException("contains lines with different widths");
            }
            prevWidth = _width;
        }
        if(_height == 0) {
            throw new IllegalArgumentException("empty input");
        }
        this.width = _width;
        this.height = _height;
        return this;
    }
    
    /**
     * Subfunction for validate to parse each line of a matrix
     * @param str
     * @return
     * @throws NoSuchElementException 
     */
    private int parseLine(String str) throws NoSuchElementException {
        int count = 0;
        Scanner sc = new Scanner(str);
        while(sc.hasNext()) {
            sc.nextDouble();
            count++;
        }
        return count;
    }
    
    /**
     * Transform input to Matrix
     * @return
     */
    @Override
    public Matrix parse() {
        if(width == -1 && height == -1) {
            return new Matrix();
        }        
        Scanner scMatrix = new Scanner(this.input);
        Matrix m = new Matrix(this.width, this.height);
        for(int i = 0; i < this.width; i++) {
            for(int j = 0; j < this.height; j++) {
                m.set(i, j, scMatrix.nextDouble());
            }
        }
        return m;
    }
}
