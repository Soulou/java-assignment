/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.unbekandt.assignment;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * Main of the applet, initialization
 * @author leo
 */
public class MainApplet extends JApplet {
    @Override
    public void init() {
        MainWindow mw = new MainWindow(true);
        add(mw.getContentPane(), BorderLayout.CENTER);
    }
}
