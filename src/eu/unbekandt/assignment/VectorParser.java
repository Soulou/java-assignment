package eu.unbekandt.assignment;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Implementation of Parser for a vector of double
 * @author leo
 */
public class VectorParser implements Parser<double[]> {
    private final String input;
    private int length;
    
    /**
     * Create parser from string input
     * @param in
     */
    public VectorParser(String in) {
      this.input = in;
      this.length = -1;
    }
    
    /**
     * Validate if the input is correct or throw exception
     * @return
     * @throws IllegalArgumentException
     */
    @Override
    public VectorParser validate() throws IllegalArgumentException {
        if(this.input.contains("\n")) {
            throw new IllegalArgumentException("multiline vector");
        }
        int _length = 0;
        Scanner sc = new Scanner(this.input);
        while(sc.hasNext()) {
            try {
                sc.nextDouble();
            } catch (NoSuchElementException e) {
                throw new IllegalArgumentException("contains something else than numbers");
            }
            _length++;
        }
        this.length = _length;
        return this;
    }
    
    /**
     * Generate vector from input
     * @return
     */
    @Override
    public double[] parse() {
        if(length == -1) {
            return new double[0];
        }        
        Scanner scVector = new Scanner(this.input);
        double[] v = new double[this.length];
        for(int i = 0; i < this.length; i++) {    
            v[i] = scVector.nextDouble();
        }
        return v;
    }
}
